<?php

class StaticController extends Controller
{
    public function mentionLegale()
    {
        return $this->twig->display('static/mentionlegales.html.twig');
    }
}
