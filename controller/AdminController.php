<?php

use Facade\Auth;

class AdminController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        // Est-ce que l'utilisateur est connecté et est admin
        if (is_null(Auth::getUser()) ||
            !(in_array('admin', Auth::getUser()['roles']) ||
                in_array('moderator', Auth::getUser()['roles']) ||
                in_array('author', Auth::getUser()['roles']))) {
            $session = Session::getInstance();
            $session->setDanger(['Vous ne pouvez pas acceder à cette section']);
            return $this->redirect('/');
        }
    }

    public function index()
    {
        // Récupère tout les messages et tout les utilisateurs
        $users = User::find_many();
        $messages = Orm::for_table(Tchat::$_table)->join(User::$_table, ['tchat.user_id', '=', 'users.id'])
            ->select_many('tchat.*', 'users.pseudo')->find_many();

        $posts = ORM::for_table(Post::$_table)->join(User::$_table, ['post.user_id', '=', 'users.id'])
            ->select_many('post.*', 'users.pseudo', ['user_slug' => 'users.slug'], 'users.roles')->find_many();
        $contacts = ContactMessage::find_many();
        //envoie a la vue les données
        return $this->twig->display('back/index.html.twig', compact('users', 'messages', 'posts', 'contacts'));
    }

    public function createUser()
    {
        if (!$this->isPost()) {
            return $this->twig->display('back/users/create.html.twig');
        }
        $session = Session::getInstance();
        // On fais le test d'enregistrement
        $result = Auth::register($_POST);
        if ($result === true) {
            // On connecte la personne apres son enregistrement
            $session->setSuccess('L\'utilisateur a bien été crée');
        } else {
            $session->setDanger($result);
        }
        return $this->twig->display('back/users/create.html.twig');
    }

    public function updateRole()
    {
        // Est-ce que c'est une methode POST
        if (!$this->isPost()) {
            $msg = ['error' => 'Methode non valide', 'success' => false];
            $this->jsonResponse($msg);
        }
        // Le token est valide ?
        if (empty($_POST['token']) || Session::getInstance()->getCsrfToken() !== $_POST['token']) {
            $msg = ['error' => 'Jeton de sécurité invalide', 'success' => false];
            $this->jsonResponse($msg);
        }


        if (empty($_POST['userid'])) {
            $msg = ['error' => 'Paramètre invalide', 'success' => false];
            $this->jsonResponse($msg);
        }
        $roles = $_POST['roles'] ?? [];
        if (count(array_diff($roles, ['admin', 'moderator', 'author'])) > 0) {
            $msg = ['error' => 'Paramètre invalide', 'success' => false];
            $this->jsonResponse($msg);
        }

        // Récupere l'utilisateur
        $user = User::find_one($_POST['userid']);

        // Verifie si l'utilisateur existe
        if (!$user) {
            $msg = ['error' => 'L\'utilisateur n\'existe pas', 'success' => false];
            $this->jsonResponse($msg);
        } else {
            // Protection pour avoir toujours un access en cas de problème
            if ((int)$user->id === 1 && (int)Auth::getUser()['id'] !== 1) {
                $msg = ['error' => 'Cet utilisateur ne peux être modifié', 'success' => false];
                $this->jsonResponse($msg);
            }
            $user->roles = serialize($roles);
            $user->save();
            $msg = ['success' => true];
            $this->jsonResponse($msg);
        }
    }

    public function updateMessage()
    {
        // Est-ce que c'est une methode POST
        if (!$this->isPost()) {
            $msg = ['error' => 'Methode non valide', 'success' => false];
            $this->jsonResponse($msg);
        }
        // Le token est valide ?
        if (empty($_POST['token']) || Session::getInstance()->getCsrfToken() !== $_POST['token']) {
            $msg = ['error' => 'Jeton de sécurité invalide', 'success' => false];
            $this->jsonResponse($msg);
        }

        if (empty($_POST['messageid'])) {
            $msg = ['error' => 'Paramètre invalide', 'success' => false];
            $this->jsonResponse($msg);
        }

        // Récupère le message
        $message = Tchat::find_one($_POST['messageid']);


        // Verifie si le message existe
        if (!$message) {
            $msg = ['error' => "Le message n'existe pas", 'success' => false];
            $this->jsonResponse($msg);
        } else {
            $message->flag = $message->flag ^ 1;
            $message->save();
            $msg = ['success' => true];
            $this->jsonResponse($msg);
        }
    }

    public function hideMessage()
    {
        if (!$this->isPost()) {
            $msg = ['error' => 'Methode non valide', 'success' => false];
            $this->jsonResponse($msg);
        }

        if (empty($_POST['token']) || Session::getInstance()->getCsrfToken() !== $_POST['token']) {
            $msg = ['error' => 'Jeton de sécurité invalide', 'success' => false];
            $this->jsonResponse($msg);
        }

        if (empty($_POST['messageid'])) {
            $msg = ['error' => 'Paramètre invalide', 'success' => false];
            $this->jsonResponse($msg);
        }

        $message = Tchat::find_one($_POST['messageid']);
        if (!$message) {
            $msg = ['error' => "Le message n'existe pas", 'success' => false];
            $this->jsonResponse($msg);
        } else {
            $message->flag = 1;
            $message->save();
            $msg = ['success' => true];
            $this->jsonResponse($msg);
        }
    }

    public function deleteAllmessage()
    {
        if (!$this->isPost()) {
            $msg = ['error' => 'Methode non valide', 'success' => false];
            $this->jsonResponse($msg);
        }

        if (empty($_POST['token']) || Session::getInstance()->getCsrfToken() !== $_POST['token']) {
            $msg = ['error' => 'Jeton de sécurité invalide', 'success' => false];
            $this->jsonResponse($msg);
        }
        
        ORM::raw_execute("TRUNCATE TABLE " . Tchat::$_table);
        $this->jsonResponse(['result' => true]);
    }


}
