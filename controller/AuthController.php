<?php

use Facade\Auth;

class AuthController extends Controller
{
    public function login()
    {
        // Si l'utilisateur est déja connecté alors on le redirige vers son profil
        if (Auth::getUser()) {
            return $this->redirect(route('profil'));
        }
        // Si c'est une requete post
        if ($this->isPost()) {
            // On instancie la classe auth
            // On récupère la session
            $session = Session::getInstance();

            // On fais le test de login
            $result = Auth::login($_POST);
            if ($result === true) {
                $session->setSuccess('Vous êtes bien connecté');
                return $this->redirect('/');
            } else {
                // On retourne les erreurs
                $session->setDanger($result);
                $email = $_POST['email'];
                return $this->twig->display('auth/login.html.twig', compact('email'));
            }
        } else {
            return $this->twig->display('auth/login.html.twig');
        }
    }

    public function register()
    {
        // Si l'utilisateur est déja connecté alors on le redirige vers son profil
        if (Auth::getUser()) {
            return $this->redirect(route('profil'));
        }

        // Si c'est une requete post
        if ($this->isPost()) {
            // On récupère la session
            $session = Session::getInstance();
            // On fais le test d'enregistrement
            $result = Auth::register($_POST);
            if ($result === true) {
                // On connecte la personne apres son enregistrement
                $session->setSuccess('Vous avez bien été enregistré');
                Auth::login(['email' => $_POST['email'], 'password' => $_POST['password']]);
                return $this->redirect(BASE_URL);
            } else {
                $session->setDanger($result);
                $pseudo = $_POST['pseudo'];
                $email = $_POST['email'];
                return $this->twig->display('auth/register.html.twig', compact('pseudo', 'email'));
            }
        } else {
            return $this->twig->display('auth/register.html.twig');
        }
    }

    public function logout()
    {
        // Déconnection de la personne
        $session = Session::getInstance();
        Auth::disconnect();
        $session->setSuccess('Vous avez bien été déconnecté');
        return $this->redirect('login');
    }
}
