<?php

use App\Twig;
use Facade\Auth;

class Controller
{

    /**
     * @var Twig_Environment
     */
    public $twig;

    public function __construct()
    {
        $this->twig = Twig::getInstance()->getTwig();
        // La personne est-elle connectée ? si non alors on la connecte si il y a un utilisateur
        if (!Auth::getUser()) {
            Auth::connectUserFromCookie();
        }
    }

    /**
     * Vérifie si c'est une méthode POST
     * @return bool
     */
    final public function isPost()
    {
        return ($_SERVER['REQUEST_METHOD'] === 'POST');
    }

    /**
     * Redirige vers un chemin
     * @param $path
     */
    final public function redirect($path)
    {
        Session::getInstance()->header("Location: $path");
        exit();
    }

    /**
     * Renvoit une réponse en json
     * @param array $data
     */
    final public function jsonResponse($data = [])
    {
        Session::getInstance()->header('Content-Type: application/json');
        echo json_encode($data);
        exit();
    }
}
