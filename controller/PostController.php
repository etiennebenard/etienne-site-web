<?php

use Carbon\Carbon;
use Facade\Auth;
use Intervention\Image\ImageManager;

class PostController extends Controller
{
    public function index()
    {
        $posts = ORM::for_table(Post::$_table)->join(User::$_table, ['post.user_id', '=', 'users.id'])
            ->select_many(
                'post.*',
                'users.pseudo',
                ['user_slug' => 'users.slug'],
                'users.roles'
            )
            ->where('published', 1)
            ->order_by_desc('created_at')->find_many();
        $currentPage = $_GET['page'] ?? 1;
		// Evite les page négatives
		if($currentPage < 1){ $currentPage = 1;}
        $limit = 5;
        $offset = ($currentPage - 1) * $limit; // offset
        $totalItems = count($posts); // total items
        $totalPages = ceil($totalItems / $limit);
        $posts = array_splice($posts, $offset, $limit);

        return $this->twig->display('post/index.html.twig', compact('posts', 'currentPage', 'totalPages'));
    }

    public function show()
    {
        $session = Session::getInstance();
        if (empty($_GET['slug'])) {
            $session->setDanger('Ce post n\'existe pas');
            return $this->redirect(route('post_index'));
        }

        //$post = Post::where('slug', $_GET['slug'])->find_one();

        $post = ORM::for_table(Post::$_table)
            ->join(User::$_table, ['post.user_id', '=', 'users.id'])
            ->select_many(
                'post.*',
                'users.pseudo',
                ['user_slug' => 'users.slug'],
                'users.roles'
            )
            ->where('slug', $_GET['slug'])
            ->find_one();

        if (!$post) {
            $session->setDanger('Ce post n\'existe pas');
            return $this->redirect(route('post_index'));
        }

        return $this->twig->display('post/show.html.twig', compact('post'));
    }

    public function edit()
    {
        $session = Session::getInstance();
        if (is_null(Auth::getUser()) ||
            !(in_array('admin', Auth::getUser()['roles']) ||
                in_array('moderator', Auth::getUser()['roles']) ||
                in_array('author', Auth::getUser()['roles']))) {
            $session->setDanger(['Vous ne pouvez pas acceder à cette section']);
            return $this->redirect(BASE_URL . route('post_index'));
        }

        if (empty($_GET['id'])) {
            $session->setDanger('Ce post n\'existe pas');
            return $this->redirect(BASE_URL . route('admin_index'));
        }

        $post = ORM::for_table(Post::$_table)
            ->join(User::$_table, ['post.user_id', '=', 'users.id'])
            ->select_many(
                'post.*',
                'users.pseudo',
                ['user_slug' => 'users.slug'],
                'users.roles'
            )
            ->find_one($_GET['id']);

        if (!$post) {
            $session->setDanger('Ce post n\'existe pas');
            return $this->redirect(BASE_URL . route('admin_index'));
        }

        if (!$this->isPost()) {
            return $this->twig->display('post/form.html.twig', compact('post'));
        }

        $validator = new Validator(new ValidatorErrorHandler());
        $validate = $validator->check($_POST, [
            'title' => [
                'required' => true,
                'minlength' => 8,
                'maxlength' => 255,
            ],
            'excerpt' => [
                'required' => true,
                'minlength' => 8,
                'maxlength' => 255,
            ],
            'body' => [
                'required' => true,
            ]
        ]);
        if (!$validate->fails()) {
            if ($_FILES['image']['name'] !== '') {
                if ($_FILES['image']['tmp_name'] !== '') {
                    $ext = strtolower(pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION));
                    if (in_array($ext, array("gif", "jpg", "png"))) {
                        if ($post->image !== null) {
                            unlink(ROOT . 'public/img/posts/' . $post->image);
                        }
                        $file_name = md5(Carbon::now() . $post->id . 'post') . ".$ext";
                        $manager = new ImageManager();
                        $manager
                            ->make($_FILES['image']['tmp_name'])
                            ->fit(640, 320)
                            ->save(ROOT . 'public/img/posts/thumb_' . $file_name);
                        copy($_FILES['image']['tmp_name'], ROOT . 'public/img/posts/' . $file_name);
                        $post->image = $file_name;
                    }
                } else {
                    $session->setDanger('Une erreur s\'est produite, l\'image n\'a pas été modifiée');
                }

                // Sauvegarde du fichier
                //suppression eventuel si fichier existe deja
            } elseif ((int)$_POST['save'] === 0) {
                // Suppression du fichier
                if ($post->image !== null) {
                    unlink(ROOT . 'public/img/posts/' . $post->image);
                    unlink(ROOT . 'public/img/posts/thumb_' . $post->image);
                    $post->image = null;
                }
            }
            $post->title = $_POST['title'];
            $post->excerpt = $_POST['excerpt'];
            $post->body = $_POST['body'];
            $post->updated_at = Carbon::now();
            $post->published = isset($_POST['published']);
            $post->save();
            $session->setSuccess('Ce post à bien été mis à jour');
            return $this->redirect('/admin');
        } else {
            $session->setDanger($validate->errors()->all());
            return $this->twig->display('post/form.html.twig');
        }
    }

    public function create()
    {
        $session = Session::getInstance();
        if (is_null(Auth::getUser()) ||
            !(in_array('admin', Auth::getUser()['roles']) ||
                in_array('moderator', Auth::getUser()['roles']) ||
                in_array('author', Auth::getUser()['roles']))) {
            $session->setDanger(['Vous ne pouvez pas acceder à cette section']);
            return $this->redirect(route('post_index'));
        }

        if (!$this->isPost()) {
            return $this->twig->display('post/form.html.twig');
        }

        $validator = new Validator(new ValidatorErrorHandler());
        $validate = $validator->check($_POST, [
            'title' => [
                'required' => true,
                'minlength' => 8,
                'maxlength' => 255,
            ],
            'excerpt' => [
                'required' => true,
                'minlength' => 8,
                'maxlength' => 255,
            ],
            'body' => [
                'required' => true,
            ]
        ]);
        if (!$validate->fails()) {
            $post = Post::create();
            if (isset($_FILES['image'])) {
                $ext = strtolower(pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION));
                if (in_array($ext, array("gif", "jpg", "png"))) {
                    $file_name = md5(Carbon::now() . $post->id . 'post') . ".$ext";
                    $manager = new ImageManager();
                    $manager->make($_FILES['image']['tmp_name'])->fit(640,
                        320)->save(ROOT . 'public/img/posts/thumb_' . $file_name);
                    copy($_FILES['image']['tmp_name'], ROOT . 'public/img/posts/' . $file_name);
                    $post->image = $file_name;
                }
            }
            $post->user_id = Auth::getUser()['id'];
            $post->title = $_POST['title'];
            $post->excerpt = $_POST['excerpt'];
            $post->body = $_POST['body'];
            $post->slug = 'slug-undefined';
            $post->created_at = Carbon::now();
            $post->published = isset($_POST['published']);
            $post->save();
			// Crée le slug avec l'id de l'article
            $post->slug = slugify($_POST['title'] . '-' . $post->id);
            $post->save();
            $session->setSuccess('Le post a bien été crée');
            return $this->redirect('/admin');
        } else {
            $session->setDanger($validate->errors()->all());
            return $this->twig->display('post/form.html.twig');
        }
    }

    public function publishPost()
    {
        // Est-ce que c'est une methode POST
        if (!$this->isPost()) {
            $msg = ['error' => 'Methode non valide', 'success' => false];
            $this->jsonResponse($msg);
        }

        if (is_null(Auth::getUser()) ||
            !(in_array('admin', Auth::getUser()['roles']) ||
                in_array('moderator', Auth::getUser()['roles']) ||
                in_array('author', Auth::getUser()['roles']))) {
            $msg = ['error' => 'Vous n\'avez pas la permission', 'success' => false];
            $this->jsonResponse($msg);
        }
        // Le token est valide ?
        if (empty($_POST['token']) || Session::getInstance()->getCsrfToken() !== $_POST['token']) {
            $msg = ['error' => 'Jeton de sécurité invalide', 'success' => false];
            $this->jsonResponse($msg);
        }

        if (empty($_POST['post'])) {
            $msg = ['error' => 'Paramètre invalide', 'success' => false];
            $this->jsonResponse($msg);
        }

        // Récupère le post
        $post = Post::find_one($_POST['post']);
        if (!$post) {
            $msg = ['error' => "Le post n'existe pas", 'success' => false];
            $this->jsonResponse($msg);
        } else {
            $post->published = $post->published ^ 1;
            $post->save();
            $msg = ['success' => true];
            $this->jsonResponse($msg);
        }
    }

    public function deletePost()
    {
        // Est-ce que c'est une methode POST
        if (!$this->isPost()) {
            $msg = ['error' => 'Methode non valide', 'success' => false];
            $this->jsonResponse($msg);
        }

        if (is_null(Auth::getUser()) ||
            !(in_array('admin', Auth::getUser()['roles']) ||
                in_array('moderator', Auth::getUser()['roles']) ||
                in_array('author', Auth::getUser()['roles']))) {
            $msg = ['error' => 'Vous n\'avez pas la permission', 'success' => false];
            $this->jsonResponse($msg);
        }
        // Le token est valide ?
        if (empty($_POST['token']) || Session::getInstance()->getCsrfToken() !== $_POST['token']) {
            $msg = ['error' => 'Jeton de sécurité invalide', 'success' => false];
            $this->jsonResponse($msg);
        }

        if (empty($_POST['post'])) {
            $msg = ['error' => 'Paramètre invalide', 'success' => false];
            $this->jsonResponse($msg);
        }

        // Récupère le post
        $post = Post::find_one($_POST['post']);
        if (!$post) {
            $msg = ['error' => "Le post n'existe pas", 'success' => false];
            $this->jsonResponse($msg);
        } else {
            if ($post->image !== null) {
                unlink(ROOT . 'public/img/posts/' . $post->image);
                unlink(ROOT . 'public/img/posts/thumb_' . $post->image);
            }
            $post->delete();
            $msg = ['success' => true];
            $this->jsonResponse($msg);
        }
    }
}
