<?php

use Facade\Auth;

class TchatController extends Controller
{
    public function tchat()
    {
        // Sélectionne les 200 derniers messages et fais une jointure avec les utilisateurs
        // pour obtenirs leurs pseudo au lieu de leur user_id
        $messages = Orm::for_table(Tchat::$_table)
            ->where('tchat.flag', '=', '0')
            ->join(User::$_table, ['tchat.user_id', '=', 'users.id'])->limit(200)
            ->select_many(['tchat.*', 'users.pseudo', 'users.slug', 'users.roles'])
            ->order_by_desc('tchat.id')->find_many();

        return $this->twig->display('tchat/tchat.html.twig', compact('messages'));
    }

    /**
     * Vérifie si un message a été posté après un autre
     * ( sert a raffraichir le tchat si un nouveau message est disponible)
     * A remplacer par des Websocket plus tard
     */
    public function getMessage()
    {
        if (!$this->isPost()) {
            $msg = ['error' => 'Method Not allowed', 'success' => false];
            $this->jsonResponse($msg);
        };

        if (!isset($_POST['currentMessagesId'])) {
            $msg = ['error' => 'Paramètre invalide', 'success' => false];
            $this->jsonResponse($msg);
        }
        $lastMessageId = json_decode($_POST['currentMessagesId']);

        // Vérifie si dans la base de donnée un message si il n'est pas archivé/ supprimé
        //On entre dans la fonction uniquement si il y a au moins un message
        if (!empty($lastMessageId)) {
            // Vérification si tout les messages ont été supprimé de la bdd via la page admin
            if (count(ORM::for_table(Tchat::$_table)->findMany()) === 0) {
                $msg = ['success' => true];
                $this->jsonResponse($msg);
            }

            $archived = ORM::for_table(Tchat::$_table)->where('flag', '1')
                ->where_in('id', $lastMessageId)->findMany();
        } else {
            $archived = false;
        }
        // Si un message est archivé alors on peut recharger
        if ($archived) {
            $msg = ['success' => true];
            $this->jsonResponse($msg);
        } else {
            // On récupère la valeur max des ids, si le tableau est vide on defini alors à 0
            $maxid = empty($lastMessageId) ? 0 : max(array_values($lastMessageId));
            $newMessage = ORM::for_table(Tchat::$_table)->where('flag', '0')
                ->whereGt('id', $maxid)->findMany();
            if ($newMessage) {
                $msg = ['success' => true];
                $this->jsonResponse($msg);
            }
            $msg = ['success' => false];
            $this->jsonResponse($msg);
        }
    }

    /**
     * Crée un nouveau message
     */
    public function newMessage()
    {
        if (!$this->isPost()) {
            $msg = ['error' => 'Method Not allowed', 'success' => false];
            $this->jsonResponse($msg);
        };

        // Vérifie si l'utilisateur est bien connecté
        if (!Auth::getUser()) {
            $msg = ['error' => 'Not allowed', 'success' => false];
            $this->jsonResponse($msg);
        }

        if (!isset($_POST['message'])) {
            $msg = ['error' => 'Incomplete request', 'success' => false];
            $this->jsonResponse($msg);
        } else {
            // Vérifie si le message est bien rempli
            if (trim($_POST['message']) === '' || strlen($_POST['message']) === 0) {
                $msg = ['error' => 'Veuillez entrer un message', 'success' => false];
                $this->jsonResponse($msg);
            }
            // Crée le nouveau message en liant l'utilisateur courrant au message
            $message = Tchat::create();
            $message->user_id = Auth::getUser()['id'];
            $message->message = $_POST['message'];
            $message->posted_at = \Carbon\Carbon::now();
            $message->save();

            $msg = ['success' => true];
            $this->jsonResponse($msg);
        }
    }
}
