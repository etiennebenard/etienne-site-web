<?php

class IndexController extends Controller
{
    public function index()
    {
        return $this->twig->display('front/index.html.twig');
    }

    public function contact()
    {
        $session = Session::getInstance();

        if (!$this->isPost()) {
            return $this->twig->display('front/contact.html.twig');
        } else {
            $validator = new Validator(new ValidatorErrorHandler());
            // Vérification si le captcha est bien envoyé dans le formulaire
            $_POST['g-recaptcha-response'] = $_POST['g-recaptcha-response'] ?? '';
            $validate = $validator->check($_POST, [
                'email' => [
                    'required' => true,
                    'email' => true
                ],
                'name' => [
                    'required' => true,
                    'alnum' => true
                ],
                'subject' => [
                    'required' => true,
                    'minlength' => 6,
                    'maxlength' => 255
                ],
                'message' => [
                    'required' => true,
                    'minlength' => 8,
                ],
                'g-recaptcha-response' => [
                    'captcha' => true
                ]
            ]);
            if ($validate->fails()) {
                $form = $_POST;
                $session->setDanger($validate->errors()->all());
                return $this->twig->display('front/contact.html.twig', compact('form'));
            } else {
                $cmsg = ContactMessage::create();
                $cmsg->email = $_POST['email'];
                $cmsg->name = $_POST['name'];
                $cmsg->subject = $_POST['subject'];
                $cmsg->message = $_POST['message'];
                $cmsg->flag = 0;
                $cmsg->save();
                $session->setSuccess('Votre message a bien été envoyé');
                return $this->redirect('/');
            }
        }
    }

    /**
     * Vérification du captcha
     */
    public function captcha()
    {
        if ($this->isPost()) {
            if (!(new Recaptcha(env('RECAPTCHA_SITEKEY'), env('RECAPTCHA_SECRETKEY')))
                ->isValid($_POST['g-recaptcha-response'])) {
                $msg = ['error' => 'Captcha invalide', 'success' => false];
            } else {
                $msg = ['email' => 'mailto:etienne.benard13@gmail.com', 'success' => true];
            }
        } else {
            $msg = ['error' => 'Non autorisé', 'success' => false];
        }

        $this->jsonResponse($msg);
    }
}
