<?php

use Facade\Auth;

class UserController extends Controller
{
    public function profil()
    {
        $session = Session::getInstance();
        // Vérifie si l'utilisateur est connecté
        if (!Auth::getUser()) {
            $session->setDanger(['Veuillez vous authentifiez']);
            return $this->redirect('login');
        }
        $userid = Auth::getUser()['id'];
        // récupère l'utilisateur
        $user = User::find_one($userid);

        // Verifie si c'est une requete POST
        if ($this->isPost()) {
            // Récupèle le profil de la personne
            $userProfile = Profil::where('user_id', $userid)->find_one();
            // Si on modifie l'avatar
            if (isset($_FILES['avatar'])) {
                // Vérifie si c'est une image
                if ($this->isFileImage($_FILES['avatar'])) {
                    $oldImg = $userProfile->avatar;
                    // Suppression de l'ancien avatar
                    if ($oldImg) {
                        unlink(ROOT . 'public/img/' . $oldImg);
                    }
                    // Copy du nouvel avatar dans le bon dossier et sauvegarder le nom dans le champ avatar
                    $file_name = md5(Carbon\Carbon::now() . $userid) . '.png';
                    copy($_FILES['avatar']['tmp_name'], ROOT . 'public/img/' . $file_name);
                    $userProfile->avatar = $file_name;
                } else {
                    $session->setDanger(['Ce n\'est pas une image']);
                    return $this->twig->display('user/profil.html.twig', compact('user'));
                }
            } else {
                // Si il y a une éditions du profil on y ajoute les champs sinon on met null
                $userProfile->firstname = $_POST['firstname'] === '' ? null : $_POST['firstname'];
                $userProfile->lastname = $_POST['name'] === '' ? null : $_POST['name'];
                $userProfile->birthday = $_POST['birthday'] === '' ? null : $_POST['birthday'];
                $userProfile->gender = $_POST['gender'] === '' ? null : $_POST['gender'];
                $userProfile->description = $_POST['description'] === '' ? null : $_POST['description'];
            }
            $userProfile->save();
            $session->setSuccess(['Votre profil a bien été sauvegardé']);
        }
        return $this->twig->display('user/profil.html.twig', compact('user'));
    }

    public function userprofil()
    {
        //vérification du nom passé dans l'url
        $name = $_GET['name'];
        // Si aucun nom est passé on redirige vers son propre profil
        if (!isset($name)) {
            return $this->redirect('profil');
        }

        // Récupération de l'utilisateur
        $user = User::where('slug', $name)->find_one();

        // Si il est inconnu on redirige vers son propre profil
        if (!$user) {
            return $this->redirect('profil');
        }

        // Vérification si c'est notre profil on redirige vers notre profil perso
        if ($user->slug === Auth::getUser()['slug']) {
            return $this->redirect('profil');
        }
        // Affichage du profil de la personne
        $profile = Profil::where('user_id', $user->id)->find_one();

        $profile->views = $profile->views + 1;
        $profile->save();
        return $this->twig->display('user/userprofil.html.twig', compact('user', 'profile'));
    }

    /**
     * Suppression de l'avatar
     */
    public function avatarDel()
    {
        // Si c'est bien une méthode POST
        if (!$this->isPost()) {
            $msg = ["error" => "Non autorisé", "success" => false];
            $this->jsonResponse($msg);
        }
        // Vérification des champs necessaire
        if (isset($_POST['user']) && isset($_POST['avatar']) && isset($_POST['token'])) {
            // Vérification du token de sécurité
            if ($_POST['token'] !== Session::getInstance()->getCsrfToken()) {
                $msg = ["error" => "Erreur de token", "success" => false];
                $this->jsonResponse($msg);
            }
            // Récupération de l'utilisateur via son slug
            $user = User::where('slug', $_POST['user'])->find_one();
            // Récupération du profil de l'utilisateur par l'id de l'utilisateur
            $userprofile = Profil::where(['user_id' => $user->id, 'avatar' => $_POST['avatar']])->find_one();
            // vérification si le profil existe
            if (!$userprofile) {
                $msg = ["error" => "Aucune correspondance entre l'avatar et l'utilisateur", "success" => false];
                $this->jsonResponse($msg);
            } else {
                // On procède a la suppression
                $avatar = $userprofile->avatar;
                $userprofile->avatar = null;
                $userprofile->save();
                unlink(ROOT . 'public/img/' . $avatar);
                $msg = ["success" => true];
                $this->jsonResponse($msg);
            }
        }
    }

    public function editCredentials()
    {
        $session = Session::getInstance();

        $currentUser = Auth::getUser();
        if (!$currentUser) {
            $session->setDanger(['Veuillez vous authentifiez']);
            return $this->redirect('login');
        }

        if (!$this->isPost()) {
            return $this->twig->display('user/edit.html.twig');
        }

        $validatorErrorHandler = new ValidatorErrorHandler();
        $validator = new Validator($validatorErrorHandler);
        $validate = $validator->check($_POST, [
            'currentpassword' => [
                'required' => true,
            ],
            'newpassword' => [
                'required' => true,
                'minlength' => 8
            ],
            'newpassword_confirm' => [
                'match' => 'newpassword'
            ]
        ]);
        if ($validate->fails()) {
            $session->setDanger($validate->errors()->all());
            return $this->twig->display('user/edit.html.twig');
        }
        $password = $_POST['currentpassword'];
        $newpassword = $_POST['newpassword'];
        $userid = $currentUser['id'];
        $user = User::find_one($userid);
        if (!$user) {
            $validatorErrorHandler->addError('Une erreur est survenue');
            $session->setDanger($validate->errors()->all());
            return $this->twig->display('user/edit.html.twig');
        }

        if (!password_verify($password, $user->password)) {
            $validatorErrorHandler->addError('Le mot de passe est incorrect');
            $session->setDanger($validate->errors()->all());
            return $this->twig->display('user/edit.html.twig');
        }

        $user->password = password_hash(
            $newpassword,
            (version_compare(PHP_VERSION, '7.2.0') >= 0)
                ? PASSWORD_ARGON2I :
                PASSWORD_BCRYPT
        );
        $user->save();
        $session->setSuccess('Le mot de passe à bien été changé. Veuillez vous reconnecter');
        Auth::disconnect();
        return $this->redirect(route('login'));
    }

    /**
     * @param array $file $_FILES
     * @return bool
     */
    private function isFileImage(array $file): Bool
    {
        $finfo = finfo_open(FILEINFO_MIME_TYPE); // Retourne le type mime à l'extension mimetype
        $mime = finfo_file($finfo, $file['tmp_name']);
        $allowed = array(
            "image/jpeg",
            "image/png",
            "image/gif",
            "image/bmp",
            "image/webp"
        );
        finfo_close($finfo);
        if (!in_array($mime, $allowed)) {
            return false;
        }
        return true;
    }
}
