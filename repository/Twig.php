<?php

namespace App;

use Stormiix\Twig\Extension\MixExtension;
use Twig\Extension\DebugExtension;

class Twig
{
    private $twig = null;
    private static $instance;

    /**
     * Twig constructor.
     */
    private function __construct()
    {
        $loader = new \Twig_Loader_Filesystem([ROOT . 'view']);
        // set up environment
        $params = array(
            'cache' => ROOT . 'cache',
            'debug' => true,
            'auto_reload' => true, // disable cache
        );
        $this->twig = new \Twig_Environment($loader, $params);
        // Ajout des extensions
        $this->twig->addExtension(new \AppExtension());
        $this->twig->addExtension(new MixExtension());
        $this->twig->addExtension(new DebugExtension());
    }

    /**
     * @return Twig_Environment|null
     */
    public function getTwig(): ?\Twig_Environment
    {
        return $this->twig;
    }

    /**
     * Retourne l'instance de la classe
     * @return Twig
     */
    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new Twig();
        }
        return self::$instance;
    }
}
