<?php

class Auth
{

    /**
     * Retourne l'utilisateur si il est connecté ou null si non
     * @return mixed|null
     */
    public function getUser()
    {
        $session = Session::getInstance();
        return $session->has('auth') ?
            $session->get('auth') :
            null;
    }

    /**
     * Enregistre une personne avec les données posté par le formulaire
     * @param $postVar
     * @return bool|array
     * @throws Exception
     */
    public function register($postVar)
    {
        $validator = new Validator(new ValidatorErrorHandler());
        // Vérification si le captcha est bien envoyé dans le formulaire
        $_POST['g-recaptcha-response'] = $_POST['g-recaptcha-response'] ?? '';
        $validate = $validator->check($_POST, [
            'email' => [
                'required' => true,
                'email' => true,
                'unique' => 'users'
            ],
            'pseudo' => [
                'required' => true,
                'minlength' => 6,
                'maxlength' => 15,
                'alnum' => true,
                'unique' => 'users'
            ],
            'password' => [
                'required' => true,
                'minlength' => 8,
            ],
            'password_confirm' => [
                'match' => 'password'
            ],
            'g-recaptcha-response' => [
                'captcha' => true
            ]
        ]);

        // Est-ce que toutes les étapes sont passé sans erreur
        if (!$validate->fails()) {
            // On crée le nouvel utilisateur avec les enregistrements
            $user = User::create();
            $user->pseudo = $postVar['pseudo'];
            $user->email = $postVar['email'];

            /*On hash le password: On utilise le hachage ARGON2i uniquement si la version de php est d'au
             moins 7.2 ( plus sécurisé) sinon on prend bcrypt*/
            $user->password = password_hash(
                $postVar['password'],
                (version_compare(PHP_VERSION, '7.2.0') >= 0) ? PASSWORD_ARGON2I : PASSWORD_BCRYPT
            );
            $user->register_date = \Carbon\Carbon::now();
            $user->slug = slugify($postVar['pseudo']);
            $user->roles = serialize([]);
            $user->save();
            $userProfile = Profil::create();
            $userProfile->user_id = $user->id;
            $userProfile->views = 0;
            $userProfile->save();
            return true;
        } else {
            // On retournes les différentes erreurs
            return $validate->errors()->all();
        }
    }

    /**
     * @param $postVar
     * @return bool|mixed
     */
    public function login($postVar)
    {
        $validatorError = new ValidatorErrorHandler();
        $validator = new Validator($validatorError);
        $validate = $validator->check($_POST, [
            'email' => [
                'required' => true,
                'email' => true
            ],
            'password' => [
                'required' => true
            ]
        ]);
        if ($validator->fails()) {
            return $validate->errors()->all();
        }

        // On check l'utilisateur via son email
        $user = User::where(['email' => $postVar['email']])->find_one();
        // L'utilisateur existe ?
        if (!$user) {
            $validatorError->addError('Nom d\'utilisateur ou mot de passe incorrect');
            return $validate->errors()->all();
        }
        // Vérification du password et connection
        if (password_verify($postVar['password'], $user->password)) {
            $time = \Carbon\Carbon::now();
            // Mise a jour de la date de derniere connection
            $user->last_connection = $time;
            $user->save();
            if (isset($postVar['remember'])) {
                $this->createRememberMe($user);
            }
            $user->roles = unserialize($user->roles);
            Session::getInstance()->set('auth', $user->as_array());
            return true;
        }
        // On renvoit les erreurs
        $validatorError->addError('Nom d\'utilisateur ou mot de passe incorrect');
        return $validate->errors()->all();
    }

    /**
     * Crée un token de connexion et crée un cookie
     * @param User $user
     */
    private function createRememberMe(User $user)
    {
        $key = Str::random(30);
        $user->rememberToken = $key;
        $user->save();
        $token = $user->id() . '==' . hash('sha256', $key . 'ssabruss' . $user->id());
        setcookie('remember_me', $token, time() + 60 * 60 * 24 * 7, "", null, true, true);
    }


    /**
     * Connecte l'utilisateur si il y a un cookie de connexion
     */
    public function connectUserFromCookie()
    {
        if (!isset($_COOKIE['remember_me'])) {
            return;
        }
        list($user_id, $token) = explode('==', $_COOKIE['remember_me']);

        $user = User::find_one($user_id);
        if (!$user) {
            $this->removeRememberCookie();
            return;
        }

        $expectedToken = hash('sha256', $user->rememberToken . 'ssabruss' . $user->id());

        if ($token !== $expectedToken) {
            $this->removeRememberCookie();
        }

        $time = \Carbon\Carbon::now();
        // Mise a jour de la date de derniere connection
        $user->last_connection = $time;
        $user->save();
        $this->createRememberMe($user);
        $user->roles = unserialize($user->roles);
        Session::getInstance()->set('auth', $user->as_array());
    }

    /**
     * Supprime le cookie de rappel de connection
     */
    public function removeRememberCookie()
    {
        setcookie('remember_me', null, -1);
        if ($this->getUser()) {
            $user = User::find_one($this->getUser()['id']);
            if ($user) {
                $user->rememberToken = null;
                $user->save();
            }
        }
    }

    public function disconnect()
    {
        $this->removeRememberCookie();
        Session::getInstance()->delete('auth');
    }
}
