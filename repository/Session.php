<?php


class Session
{
    /**
     * Session constructor.
     */
    private function __construct()
    {
        if (session_status() === PHP_SESSION_NONE) {
            session_start();
        }
    }

    /**
     * @var Session
     */
    private static $instance;

    /**
     * @return Session
     */
    public static function getInstance(): \Session
    {
        if (!self::$instance) {
            self::$instance = new Session();
        }
        return self::$instance;
    }

    /**
     * @param $key
     * @param $value
     */
    public function set($key, $value): void
    {
        $_SESSION[$key] = $value;
    }

    /**
     * @param $key
     * @return mixed|null
     */
    public function get($key)
    {
        return $this->has($key) ? $_SESSION[$key] : null;
    }

    /**
     * @param $key
     * @return bool
     */
    public function has($key): bool
    {
        return isset($_SESSION[$key]);
    }

    /**
     * @param $key
     */
    public function delete($key): void
    {
        unset($_SESSION[$key]);
    }

    /**
     * @param $string
     * @param bool $replace
     * @param null $http_response_code
     */
    public function header($string, $replace = true, $http_response_code = null)
    {
        header($string, $replace, $http_response_code);
    }


    /**
     * Génére ou vérifie le token de sécurité
     * @return mixed|null
     */
    public function getCsrfToken()
    {
        if (!$this->has('csrf_token')) {
            $this->set('csrf_token', base64_encode(openssl_random_pseudo_bytes(32)));
        }
        return $this->get('csrf_token');
    }

    /**
     * Défini un message flash de danger
     * @param $value
     */
    public function setDanger($value)
    {
        $this->set('dangerFlash', $value);
    }

    /**
     * Défini un message flash de success
     * @param $value
     */
    public function setSuccess($value)
    {
        $this->set('successFlash', $value);
    }
}
