<?php

class Validator
{
    private $rules = ['required', 'minlength', 'maxlength', 'email', 'alnum', 'match', 'unique','captcha'];

    private $messages = [
        'required' => 'Le champ :field est requis',
        'minlength' => 'Le champ :field doit faire :satisifer caractères minimum',
        'maxlength' => 'Le champ :field doit faire :satisifer caractères maximum',
        'email' => 'Ce n\'est pas une adresse email valide',
        'alnum' => 'Le champ :field doit être de type alphanumérique et peux contenir des tirets',
        'match' => 'Les champs :field et :satisifer doivent être identiques',
        'unique' => 'Le champ :field a déjà été pris',
        'captcha' => 'Le captcha est invalide'
    ];

    private $items;

    /**
     * @var ValidatorErrorHandler
     */
    private $validatorErrorHandler;


    public function __construct(ValidatorErrorHandler $validatorErrorHandler)
    {
        $this->validatorErrorHandler = $validatorErrorHandler;
    }

    /**
     * @param array $items
     * @param array $rules
     * @return Validator
     */
    public function check(array $items, array $rules): Validator
    {
        $this->items = $items;
        foreach ($items as $item => $value) {
            if (in_array($item, array_keys($rules))) {
                $this->validate([
                    'field' => $item,
                    'value' => $value,
                    'rules' => $rules[$item]
                ]);
            }
        }
        return $this;
    }

    public function fails()
    {
        return $this->validatorErrorHandler->hasErrors();
    }

    /**
     * @return ValidatorErrorHandler
     */
    public function errors(): ValidatorErrorHandler
    {
        return $this->validatorErrorHandler;
    }

    /**
     * @param array $item
     */
    private function validate(array $item)
    {
        $field = $item['field'];
        foreach ($item['rules'] as $rule => $satisifer) {
            if (in_array($rule, $this->rules)) {
                if (!call_user_func_array([$this, $rule], [$field, $item['value'], $satisifer])) {
                    $this->validatorErrorHandler->addError(
                        str_replace([':field', ':satisifer'], [$field, $satisifer], $this->messages[$rule]),
                        $field
                    );
                }
            }
        }
    }

    private function required($field, $value, $satisifer)
    {
        return !empty(trim($value));
    }

    private function minlength($field, $value, $satisifer)
    {
        return mb_strlen($value) >= $satisifer;
    }

    private function maxlength($field, $value, $satisifer)
    {
        return mb_strlen($value) <= $satisifer;
    }

    private function email($field, $value, $satisifer)
    {
        return filter_var($value, FILTER_VALIDATE_EMAIL);
    }

    private function alnum($field, $value, $satisifer)
    {
        $valid = array('-', '_');
        return ctype_alnum(str_replace($valid, '', $value));
    }

    private function match($field, $value, $satisifer)
    {
        return $value === $this->items[$satisifer];
    }

    private function unique($field, $value, $satisifer)
    {
        return !Orm::for_table($satisifer)->where($field, $value)->find_one();
    }

    private function captcha($field, $value, $satisifer)
    {
        return (new Recaptcha(env('RECAPTCHA_SITEKEY'), env('RECAPTCHA_SECRETKEY')))->isValid($value);
    }
}
