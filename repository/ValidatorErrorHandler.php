<?php

class ValidatorErrorHandler
{

    private $errors = [];

/**
* @param string $error
* @param null|string $key
*/
    public function addError(string $error, ?string $key = null)
    {
        if ($key) {
            $this->errors[$key][] = $error;
        } else {
            $this->errors[] = $error;
        }
    }

/**
* @param null|string $key
* @return array
*/
    public function all(?string $key = null): array
    {
        return isset($this->errors[$key]) ? $this->errors[$key] : $this->errors;
    }

/**
* @return bool
*/
    public function hasErrors(): bool
    {
        return count($this->all()) ? true : false;
    }

/**
* @param string $key
* @return string
*/
    public function first(string $key): string
    {
        return isset($this->all()[$key][0]) ? $this->all()[$key][0] : '';
    }
}
