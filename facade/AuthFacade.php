<?php
namespace Facade;

class Auth
{
    public static function __callStatic($name, $arguments)
    {
        $auth = new \Auth();
        return call_user_func_array([$auth, $name], $arguments);
    }
}
