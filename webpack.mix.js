const mix = require('laravel-mix');
const autoPrefixer = require('autoprefixer')

mix
    .setPublicPath('public')
    .autoload({
        'jquery': ['jQuery', '$'],
    })
    .js('assets/js/app.js', 'bundles/js')
    .js('assets/js/admin.js', 'bundles/js')
    .copyDirectory('node_modules/tinymce/skins/lightgray', 'public/bundles/css/skins/lightgray')
    .copy('node_modules/tinymce-i18n/langs/fr_FR.js', 'public/bundles/languages/tinyMCE')
    .sass('assets/scss/app.scss', 'bundles/css')
    .options({
        postCss: [
            autoPrefixer({
                browsers: [
                    '> 1%',
                    'last 2 versions',
                    'ie >= 11'
                ],
                grid: true
            }
        )]
    })
    .version();
