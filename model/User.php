<?php

class User extends Model
{
    public static $_table = 'users';

    // Récupération du profil de l'utilisateur
    public function getUserProfil()
    {
        return $this->has_one(Profil::class, 'user_id');
    }

    public function getNombreMessagesPost()
    {
        return Orm::for_table(Tchat::$_table)->where('user_id', $this->id())->count();
    }
}
