import $ from 'jquery'
import 'popper.js'
import 'bootstrap'
import '@fortawesome/fontawesome-free/js/all.min'
import 'cookieconsent'
import swal from 'sweetalert'
import tinymce from 'tinymce/tinymce'
import 'tinymce/themes/modern'
import 'tinymce/plugins/image'
import 'tinymce/plugins/link'
import 'tinymce/plugins/imagetools'
import 'tinymce/plugins/code'
import 'tinymce/plugins/fullscreen'
import 'tinymce/plugins/lists'
import 'tinymce/plugins/preview'
import 'bootstrap-fileinput/js/plugins/piexif.min'
import 'bootstrap-fileinput/js/plugins/purify.min'
import 'bootstrap-fileinput'
import 'details-polyfill'

window.$ = $
window.jQuery = $
require('bootstrap-fileinput/themes/fas/theme.min')
require('bootstrap-fileinput/js/locales/fr')


// Défini le temps de recherche de nouveau message du tchat
const tchatTimer = 1500
$(function () {
    const csrfToken = $('meta[name="csrf-token"]').attr('content')
    /*
        Cookie consent
     */
    window.cookieconsent.initialise({
        "palette": {
            "popup": {
                "background": "#237afc"
            },
            "button": {
                "background": "#fff",
                "text": "#237afc"
            }
        },
        "theme": "classic",
        "position": "bottom-right",
        "content": {
            "message": "Ce site utilise des cookies pour vous assurer la meilleure expérience sur notre site.",
            "dismiss": "Valider",
            "link": "Learn more"
        }
    })


// Au click sur le bouton edit
    $('#editProfile').on('click', function () {
        // On modifie l'aspect du bouton edit
        $(this).addClass('btn-primary').removeClass('btn-secondary')
        // On supprime les readonly des inputs
        $('#formProfile input[readonly],textarea[readonly]')
            .each(function () {
                $(this).removeAttr("readonly")
                $(this).removeClass('form-control-plaintext').addClass('form-control')
            })
        $('#formProfile select option[disabled]').removeAttr("disabled")
        // On afiche le bouton valider
        $('#formProfile button[type=submit]').parent().removeClass('d-none')

    })
// Au changement d'avatar on affiche le bouton valider
    $('input#avatar').on('change', function () {
        $(this).next('button').removeClass('d-none')
    })

// Au clic sur le bouton de suppréssion de l'avatar
    $('#avatarDel').on('click', async function (e) {
        e.preventDefault()
        const img = $(this).parent().find('#avatarImg img')
        const user = img.data('user')
        const avatar = img.data('avatar')
        const defaultAvatar = '/public/img/defaultAvatar.png'
        // On fais la requete de suppression
        let result = await swal({
            title: "Êtes-vous sur de vouloir supprimer votre avatar?",
            icon: "warning",
            buttons: ['Annuler', 'Valider'],
            dangerMode: true,
        })
        if (result) {
            $.post(
                '/avatarDel',
                {user, avatar, token: csrfToken},
                function (data) {
                    // Si c'est un success alors on modifie visuellement l'aspect
                    if (data.success) {
                        swal("Votre avatar à bien été supprimé!", {icon: "success"})
                        $('#avatarImg img').attr('src', defaultAvatar)
                        $('#avatarDel').remove()
                    } else {
                        swal("Un problème est survenu!", data.error, "error")
                    }
                }
            )
        }
    })
    $.fn.reverse = [].reverse

    if ($('#tchat').length > 0) {
        // Auto Reload tchat container
        setInterval(() => {
            let currentMessages = $('.message-container .message')
            var currentMessagesId = []
            $(currentMessages).each(function () {
                if ($(this).data('message-id')) {
                    currentMessagesId.push($(this).data('message-id'))
                }
            })
            currentMessagesId = JSON.stringify(currentMessagesId)
            // Requete pour déterminer si il y a un nouveau message
            $.post('/getMessage', {currentMessagesId}, function (data) {
                if (data.success) {
                    // Si oui alors on reload le container du tchat
                    $('#tchat').load(document.URL + ' .message-container', function () {
                        // On scroll en bas pour voir le nouveau message
                        $(".message-container").scrollTop($(".message-container")[0].scrollHeight)
                    })
                }
            })
        }, tchatTimer)

        // A l'envoit du nouveau message
        $("#postMessage").on('submit', function (e) {
            e.preventDefault()
            const form = $(this)
            // On envoit la requete avec le message
            $.post(
                form.attr('action'),
                form.serialize(),
                function (data) {
                    // Si c'est un success on scroll
                    if (data.success) {
                        $(".message-container").scrollTop($(".message-container")[0].scrollHeight)

                        /* Bloque le flood en désactivant l'input*/
                        form.find('input').attr('disabled', 'disabled')
                        form.find('.input-group-append button').attr('disabled', 'disabled')
                        form.find('input').val("")
                        setTimeout(function () {
                            form.find('input').removeAttr('disabled')
                            form.find('.input-group-append button').removeAttr('disabled')
                            form.find('input').focus()
                        }, 3000)
                    } else {
                        // On affiche une erreur en cas de problème
                        swal("Un problème est survenu!", data.error, "error")
                        form.find('input').val("")
                        form.find('input').focus()
                    }
                }
            )
        })

        // Permet de bind l'event après le load
        $(document).on('click', '.message-hide', function (e) {
            e.preventDefault()
            let messageid = $(this).parents().eq(1).data('message-id')
            $.post('/hideMessage', {messageid, token: csrfToken}, function (data) {
                if (data.success) {
                    // Si oui alors on reload le container du tchat
                    $('#tchat').load(document.URL + ' .message-container', function () {
                        $(".message-container").scrollTop($(".message-container")[0].scrollHeight)
                    })
                }
            })
        })
    }

    tinymce.init({
        selector: '.tinymce',
        plugins: 'image link imagetools code fullscreen lists preview',
        theme: 'modern',
        height: '20vh',
        skin_url: '/public/bundles/css/skins/lightgray',
        element_format: 'html',
        invalid_elements: "script,object,embed,link,style,form,input,iframe",
        branding: false,
        language_url: '/public/bundles/languages/tinyMCE/fr_FR.js',
        language: 'fr_FR',
        setup: function (editor) {
            editor.on('change', function () {
                editor.save();
            });
        },
        // without images_upload_url set, Upload tab won't show up
        images_upload_url: '/tinymceUpload.php',

        // override default upload handler to simulate successful upload
        images_upload_handler: function (blobInfo, success, failure) {
            var xhr, formData;

            xhr = new XMLHttpRequest();
            xhr.withCredentials = false;
            xhr.open('POST', '/tinymceUpload.php');

            xhr.onload = function () {
                var json;

                if (xhr.status != 200) {
                    failure('HTTP Error: ' + xhr.status);
                    return;
                }

                json = JSON.parse(xhr.responseText);

                if (!json || typeof json.location != 'string') {
                    failure('Invalid JSON: ' + xhr.responseText);
                    return;
                }
                success('/' + json.location);
            };

            formData = new FormData();
            formData.append('file', blobInfo.blob(), blobInfo.filename());

            xhr.send(formData);
        },
    })
})



