import $ from 'jquery'
import 'bootstrap'
import '@fortawesome/fontawesome-free/js/all'
import 'select2/dist/js/select2.min'
import 'datatables.net-bs4'
import swal from 'sweetalert'
window.$ = $
window.jQuery = $

$(function () {
    const csrfToken = $('meta[name="csrf-token"]').attr('content')

    $("#nav-tab").find('a').first().tab('show')

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust()
    })

    $('.roles').select2({
        width: '100%'
    })

    $('#role_user').on('change', function () {
        userTable.draw()
    })

    //========================================
    // DATATABLE
    //========================================

    $.fn.dataTable.ext.search.push(
        function (settings, searchData, index, rowData, counter) {
            if (settings.sTableId === 'user_table') {
                let value = $('#role_user option:selected').val()
                if (value !== "") {
                    let row = userTable.row(index).nodes().to$()
                    let elSelectedValue = []
                    $.each($(row[0]).find('select option:selected'), function () {
                        elSelectedValue.push($(this).val())
                    })
                    return ($.inArray(value, elSelectedValue) !== -1)
                }
            }
            return true
        }
    )


    $.extend($.fn.dataTable.ext.classes, {
        sLengthSelect: "custom-select custom-select-sm",
    })

    $.extend($.fn.dataTable.defaults, {
        language: {
            "sProcessing": "Traitement en cours...",
            "sSearch": "Rechercher&nbsp;:",
            "sLengthMenu": "Afficher _MENU_ &eacute;l&eacute;ments",
            "sInfo": "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            "sInfoEmpty": "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            "sInfoFiltered": "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            "sInfoPostFix": "",
            "sLoadingRecords": "Chargement en cours...",
            "sZeroRecords": "Aucun &eacute;l&eacute;ment &agrave; afficher",
            "sEmptyTable": "Aucune donn&eacute;e disponible dans le tableau",
            "oPaginate": {
                "sFirst": "Premier",
                "sPrevious": "Pr&eacute;c&eacute;dent",
                "sNext": "Suivant",
                "sLast": "Dernier"
            },
            "oAria": {
                "sSortAscending": ": activer pour trier la colonne par ordre croissant",
                "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
            }
        }
    })

    let userTable = $('#user_table').DataTable({
        orderClasses: false,
        scrollX: true,
        columnDefs: [
            {"type": "num", "targets": 0}
        ],
        drawCallback: function () {
            userEvent()
        }
    })
    let postTable = $('#post_table').DataTable({
        columns: [
            {width: '5%'},
            {width: '10%'},
            {width: '42%'},
            {width: '14%'},
            {width: '14%'},
            {width: '5%'},
            {width: '10%'}
        ],
        orderClasses: false,
        scrollX: true,
        order: [[3, 'desc']],
        columnDefs: [
            {"type": "num", "targets": 0}
        ],
        drawCallback: function () {
            postEvent()
        }
    })
    let tchatTable = $('#tchat_table').DataTable({
        columns: [
            {width: '8%'},
            {width: '12%'},
            {width: '60%'},
            {width: '15%'},
            {width: '5%'},
        ],
        scrollX: true,
        orderClasses: false,
        order: [[0, "desc"]],
        columnDefs: [
            {"type": "num", "targets": 0}
        ],
        drawCallback: function () {
            tchatEvent()
        }
    })
    let contactTable = $('#contact_table').DataTable({
        scrollX: true,
        orderClasses: false,
        order: [[0, "desc"]],
        columnDefs: [
            {"type": "num", "targets": 0}
        ],
        drawCallback: function () {
        }
    })



    function userEvent() {
        $('.updateRole').off('change').on('change', function (e) {
            const select = $(this)
            const td = select.parent()
            const roles = select.val()
            const userid = select.data('user')
            $.post('/updateRole', {
                    userid,
                    roles,
                    token: csrfToken
                },
                function (data) {
                    if (data.success) {
                        select.select2({
                            data: roles,
                            width: '100%'
                        })
                        userTable.draw()
                    } else {
                        swal("Un problème est survenu!", data.error, "error")
                    }
                }
            )
        })
    }

    function tchatEvent() {
        $('.updateMessage').off('click').on('click', function (e) {
            e.preventDefault()
            const button = $(this)
            const messageid = button.data('message')
            $.post('/updateMessage', {
                    messageid,
                    token: csrfToken
                },
                function (data) {
                    if (data.success) {
                        if (button.hasClass('btn-success')) {
                            button.removeClass('btn-success').addClass('btn-danger')
                            button.html('<i class="fas fa-times fa-fw"></i>')
                            button.attr('title', "Afficher le message")
                            button.parent().get(0).setAttribute('data-order', 1)
                            tchatTable.cell(button.parent()).invalidate().draw()
                        } else {
                            button.addClass('btn-success').removeClass('btn-danger')
                            button.html('<i class="fas fa-check fa-fw"></i>')
                            button.attr('title', "Supprimer le message")
                            button.parent().get(0).setAttribute('data-order', 0)
                            tchatTable.cell(button.parent()).invalidate().draw()
                        }
                    } else {
                        swal("Un problème est survenu!", data.error, "error")
                    }
                }
            )
        })
    }

    function postEvent() {
        $('.post_publish').off('change').on('change', function (e) {
            e.preventDefault()
            const checkbox = $(this)
            const postID = checkbox.data('post')
            $.post('/publishPost', {
                    post: postID,
                    token: csrfToken
                },
                function (data) {
                    if (!data.success) {
                        swal("Un problème est survenu!", data.error, "error")
                        checkbox.prop('checked', !checkbox.is(':checked'))
                    } else {
                        checkbox.parents().get(1).setAttribute('data-order', checkbox.is(':checked') ? 1 : 0)
                        postTable.cell(checkbox.parents().eq(1)).invalidate().draw()
                    }
                })
        })

        $('.delete_post').on('click', async function (e) {
            e.preventDefault()
            const tr = $(this).parents().eq(1)
            const postID = $(this).data('post')
            let result = await swal({
                title: "Êtes-vous sur de vouloir supprimer ce post?",
                text: "Ceci est irréversible",
                icon: "warning",
                buttons: ['Annuler', 'Valider'],
                dangerMode: true,
            })
            if (result) {
                $.post('/deletePost', {
                    post: postID,
                    token: csrfToken
                }, function (data) {
                    if (data.success) {
                        swal("Le post à bien été supprimé!", {icon: "success"})
                        postTable.row(tr).remove().draw()
                    } else {
                        swal("Un problème est survenu!", data.error, "error")
                    }
                })
            }
        })
    }

    $('#deleteAllMessages').on('click', async function () {
        const tr = $(this).parents().eq(1)
        let result = await swal({
            title: "Êtes-vous sur?",
            text: "Vous ne pourrez pas restaurer les messages!",
            icon: "warning",
            buttons: ['Annuler', 'Valider'],
            dangerMode: true,
        })
        if (result) {
            try {
                let formData = new FormData();
                formData.append("token", csrfToken);
                let response = await fetch('/deleteAllmessage', {
                    method: 'POST',
                    body: formData
                })
                let json = await response.json()

                if (json.result) {
                    await swal("Tout les messages ont été supprimé!", {
                        icon: "success",
                    })
                    tchatTable.clear().draw()
                    tr.remove()
                } else {
                    await swal("Un problème est survenu!", json.error, "error")
                }
            } catch (e) {
                await swal("Un problème est survenu!", e, "error")
            }
        }
    })
})