<?php

use Carbon\Carbon;
use Facade\Auth;

class AppExtension extends Twig_Extension implements Twig_Extension_GlobalsInterface
{
    public function getGlobals()
    {
        return array(
            'session' => \Session::getInstance(),
            'BASE_URL' => BASE_URL,
            'ROOT' => ROOT,
            'User' => Auth::getUser()
        );
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('assets', array($this, 'assets')),
            new \Twig_SimpleFunction('route', array($this, 'route')),
            new \Twig_SimpleFunction('localiseDate', array($this, 'localiseDate'), ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('getenv', array($this, 'getenv')),
            new \Twig_SimpleFunction('age', array($this, 'age')),
            new \Twig_SimpleFunction('isGranted', array($this, 'isGranted')),
            new \Twig_SimpleFunction('activePage', array($this, 'activePage')),
        );
    }

    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('unserialize', [$this, 'unserialize']),
        ];
    }

    public function assets($path)
    {

        return BASE_URL . 'public' . $path;
    }

    public function route($route)
    {
        $routes = require ROOT . 'routes.php';
        foreach ($routes as $key => $_route) {
            if (is_array($_route)) {
                if ($_route['name'] === $route) {
                    return BASE_URL . $key;
                }
            }
        }
        throw new Exception("Unknown named route");
    }

    public function localiseDate($date, string $format = '%a %d %b %Y &agrave; %X')
    {
        return Carbon::parse($date)->formatLocalized($format);
    }

    public function getenv($string, $default = null)
    {
        return env($string, $default);
    }

    public function age($date)
    {
        $birthday = Carbon::parse($date);
        $now = Carbon::now();
        return $now->diffInYears($birthday);
    }

    public function unserialize($string)
    {
        return unserialize($string);
    }

    public function isGranted($role, $object = null)
    {
        if (is_null($object)) {
            $object = Auth::getUser()['roles'] ?? [];
        }

        return in_array($role, $object);
    }

    public function activePage($url)
    {

        return basename($_SERVER['REQUEST_URI']) === $url;
    }
}
