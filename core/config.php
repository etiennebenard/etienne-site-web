<?php

use Carbon\Carbon;
use Dotenv\Dotenv;
use App\Twig;

/*
    Check la version de php
 */

if (version_compare(PHP_VERSION, '7.0.0') < 0) {
    die("Version de php incompatible avec l'application (php 7.0.0 minimum, 7.2.0 recommandée)");
}

/*
  Composer Autoload
*/

require ROOT . 'vendor/autoload.php';

/*
  Require helpers
*/
require ROOT . 'core/helpers.php';

/*
   Demarrer la session
*/
Session::getInstance();

/*
  Variable environnement
  gestion .env
 */

$env = new Dotenv(dirname(__DIR__));
$env->load();


/*
 Configuration ORM
*/

ORM::configure('mysql:host=' . env('DB_HOST', 'localhost') . ';dbname=' . env('DB_NAME'));
ORM::configure('username', env('DB_USER', 'root'));
ORM::configure('password', env('DB_PASSWORD', ''));
ORM::configure('driver_options', [PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8']);

/*
 Configuration CARBON et Dates
*/
date_default_timezone_set('Europe/Paris');
setlocale(LC_TIME, 'fr');
Carbon::setLocale('fr');
Carbon::setUtf8(true);

/*
Configuration Twig
*/
Twig::getInstance();
