<?php

use Carbon\Carbon;

/**
 * Debug and Die
 */
function dd()
{
    array_map(function ($x) {
        var_dump($x);
    }, func_get_args());

    die(1);
}

/**
 * @param $date
 * @param string $format
 * @return string
 */
function localiseDate($date, string $format = '%a %d %b &agrave; %X')
{
    return Carbon::parse($date)->formatLocalized($format);
}

if (!function_exists('env')) {
    /**
     * Gets the value of an environment variable. Supports boolean, empty and null.
     *
     * @param  string $key
     * @param  mixed $default
     * @return mixed
     */
    function env($key, $default = null)
    {
        $value = getenv($key);

        if ($value === false) {
            return value($default);
        }

        switch (strtolower($value)) {
            case 'true':
            case '(true)':
                return true;
            case 'false':
            case '(false)':
                return false;
            case 'empty':
            case '(empty)':
                return '';
            case 'null':
            case '(null)':
                return;
        }

        if (strlen($value) > 1 && Str::startsWith($value, '"') && Str::endsWith($value, '"')) {
            return substr($value, 1, -1);
        }

        return $value;
    }
}

/**
 * Retourne l'url à partir d'un nom d'une route
 * @param $route
 * @return string
 * @throws Exception
 */
function route($route)
{
    $routes = require ROOT . 'routes.php';
    foreach ($routes as $key => $_route) {
        if (is_array($_route)) {
            if ($_route['name'] === $route) {
                return $key;
            }
        }
    }
    throw new Exception("Unknown named route");
}


/**
 * Slugify string for url
 * @param $string
 * @param array $replace
 * @param string $delimiter
 * @return mixed|null|string|string[]
 * @throws Exception
 */
function slugify($string, $replace = array(), $delimiter = '-')
{
    // https://github.com/phalcon/incubator/blob/master/Library/Phalcon/Utils/Slug.php
    if (!extension_loaded('iconv')) {
        throw new Exception('iconv module not loaded');
    }
    $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $string);
    if (!empty($replace)) {
        $clean = str_replace((array)$replace, ' ', $clean);
    }
    $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
    $clean = strtolower($clean);
    $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
    $clean = trim($clean, $delimiter);
    return $clean;
}
