<?php

class Routing
{
    /*
     * Prise en charge de la page appelée et rediriger la logique au bon endroit
     */
    public static function handle(): void
    {
        // Require du fichiers avec toute les routes
        $routes = require ROOT . 'routes.php';
        // Vérification si la route existe , si oui alors on détermine son controlleur et l'action,
        // si non on sort une 404 Not Found
        $p = trim($_GET['p'] ?? '', '/');
        if (array_key_exists($p, $routes)) {
            $to_call = is_array($routes[$p]) ?
                $routes[$p]['action'] :
                $routes[$p];
        } else {
            self::notFound();
        }

        /*
         * Gestion du controller a appeller
         */
        $controller_to_call = substr($to_call, 0, strpos($to_call, '@'));
        $action_to_call = substr($to_call, strpos($to_call, '@') + 1);
        $controller = new $controller_to_call();
        $controller->$action_to_call();
    }

    public static function notFound()
    {
        Session::getInstance()->header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found', true, 404);
        $code = 404;
        include(ROOT . 'view/errors/error.php');
        exit();
    }
}
