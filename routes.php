<?php
/*
 'url' => ['action' => 'Controller@methode','name' => 'nom de la route'],
 'url' => Controller@methode
 */

return [
    /* Racine du site */
    '' => ['action' => 'IndexController@index', 'name' => 'index'],
    'contact' => ['action' => 'IndexController@contact', 'name' => 'contact'],

    /* Connection */
    'login' => ['action' => 'AuthController@login', 'name' => 'login'],
    'register' => ['action' => 'AuthController@register', 'name' => 'register'],
    'logout' => ['action' => 'AuthController@logout', 'name' => 'logout'],

    /* User */
    'profil' => ['action' => 'UserController@profil', 'name' => 'profil'],
    'user' => ['action' => 'UserController@userprofil', 'name' => 'userprofil'],
    'editCredentials' => ['action' => 'UserController@editCredentials', 'name' => 'editCredentials'],

    /* TChat */
    'tchat' => ['action' => 'TchatController@tchat', 'name' => 'tchat'],
    'newMessage' => ['action' => 'TchatController@newMessage', 'name' => 'newMessage'],

    /*  Page Statique */
    'mentions-legales' => ['action' => 'StaticController@mentionLegale', 'name' => 'mentionslegales'],

    /*  Post */
    'posts' => ['action' => 'PostController@index', 'name' => 'post_index'],
    'post' => ['action' => 'PostController@show', 'name' => 'post_show'],
    'post/edit' => ['action' => 'PostController@edit', 'name' => 'post_edit'],
    'post/new' => ['action' => 'PostController@create', 'name' => 'post_create'],

    /* Admin */
    'admin' => ['action' => 'AdminController@index', 'name' => 'admin_index'],
    'admin/user/new' => ['action' => 'AdminController@createUser', 'name' => 'admin_user_create'],

    /* XHR */
    'avatarDel' => 'UserController@avatarDel',
    'getMessage' => 'TchatController@getMessage',
    'checkCaptcha' => 'IndexController@captcha',
    'updateRole' => 'AdminController@updateRole',
    'updateMessage' => 'AdminController@updateMessage',
    'hideMessage' => 'AdminController@hideMessage',
    'deleteAllmessage' => 'AdminController@deleteAllmessage',
    'publishPost' => 'PostController@publishPost',
    'deletePost' => 'PostController@deletePost'

];
