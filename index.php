<?php

/*
 * DÉFINITIONS DES CONSTANTES
 */
define('ROOT', str_replace('index.php', '', $_SERVER['SCRIPT_FILENAME']));

define('BASE_URL', substr($_SERVER['SCRIPT_NAME'], 0, strrpos($_SERVER['SCRIPT_NAME'], '/')+1));

/*
 * INCLUSIONS DE LA CONFIGURATION
 */
require ROOT . 'core/config.php';

/*
 * Gestion des routes pour rediriger vers la bonne page
 */
Routing::handle();
